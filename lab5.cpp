/*
Parker Myslow
pmyslow@clemson.edu
Lab 5
Lab Section: 4
TA: Nushrat
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

	//Creates a deck of 52 elements (0-51)
	Card deck[52];

	//Initializes 2 to Ace for all Spades cards
	for(int i = 0; i < 13; i++)
	{
		deck[i].suit = SPADES;
		deck[i].value = i + 2;
	}

	//Initializes 2 to Ace for all Hearts cards
	for(int i = 13; i < 26; i++)
	{
		deck[i].suit = HEARTS;
		deck[i].value = i - 11;
	}
	
	//Initializes 2 to Ace for all Diamonds cards
	for(int i = 26; i < 39; i++)
	{
		deck[i].suit = DIAMONDS;
		deck[i].value = i - 24;
	}
	
	//Initializes 2 to Ace for all Clubs cards
	for(int i = 39; i < 52; i++)
	{
		deck[i].suit = CLUBS;
		deck[i].value = i - 37;
	}
	
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

	//Shuffles the deck from beginning to end using randomizer function provided
	random_shuffle(deck, deck + 52, myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

	//Creates a hand of 5 elements (0-4)
	Card hand[5];
	
	//Sets the hand equal to the first 5 elements of the randomly shuffled deck
	for(int j = 0; j < 5; j++)
	{
		hand[j] = deck[j];
	}

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

	//Sorts the hand of 5 randomly picked cards first based off of suit then value
	sort(hand, hand + 5, suit_order);
		


    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
	
	//Goes through each card in the hand
	for(int z = 0; z < 5; z++)
	{
		//Checks if it is a number card
		if(hand[z].value < 11)
		{
			cout << hand[z].value << " of " << get_suit_code(hand[z]) << endl;
		}

		//Checks if it is a face card
		if(hand[z].value >= 11)
		{
			cout << get_card_name(hand[z]) << " of " << get_suit_code(hand[z]) << endl;
		}
	}




  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  	
	//Logic checks each card for priority over other cards first by suit then number
	if(lhs.suit < rhs.suit)
	{
		return true;
	}
	
	if(lhs.suit == rhs.suit)
	{
		if(lhs.value < rhs.value)
		{
			return true;
		}
		
		else 
		{
			return false;
		}
	}
	
	return false;
}

string get_suit_code(Card& c) {
  
  //Changes each suit enum value to a unicode value in the terminal
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {

  //Changes each face card integer value to their respective names
  switch (c.value){
    case 11:        return "Jack";
    case 12:        return "Queen";
    case 13:        return "King";
    case 14:        return "Ace";
    default:        return "";
  }
}
